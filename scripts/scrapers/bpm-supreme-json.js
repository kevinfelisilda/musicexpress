require('dotenv').config()

const fs = require('fs')
const chalk = require('chalk')
const moment = require('moment')
const Track = require('../../server/models').track
const TrackVersion = require('../../server/models').track_version

const atob = (a) => {
  return new Buffer(a, 'base64').toString('binary');
}

const btoa = (b) => {
  return new Buffer(b).toString('base64')
}

const timeout = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

const readJsonLine = (line) => {
  try {
     const parsedLine = JSON.parse(line)

     return parsedLine
  } catch (err) {
    return null
  }
}

const hasValidTracks = (obj) => {
  if (typeof obj === 'object' &&
    obj &&
    obj.id &&
    obj.tracks &&
    Array.isArray(obj.tracks) &&
    obj.tracks.length > 0
  ) {
    return true
  }

  return false
}


/*
{
  id: 12,
  tracks: [{
    iMediaID: '23',
    vArtistName: 'The Black Eyed Peas',
    iAlbumID: '12',
    downloadCount: '0',
    vMediaFile: 'aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2JwbXBoYXNlMi9hdWRpby9UaGUgVGltZSBbVGhlIERpcnR5IEJpdF0gW01haW5dLm1wMw==',
    vPreviewFile: 'aHR0cHM6Ly9kM2NhZ293Mjl2NHcyNy5jbG91ZGZyb250Lm5ldC9hdWRpby9UaGUgVGltZSBbVGhlIERpcnR5IEJpdF0gW01haW5dLm1wMw==',
    vMediaName: 'The Time (The Dirty Bit) (Clean)',
    vVideoThumb: '',
    iAllowDownload: '0',
    dAddedDate: '07/25/11',
    iBPMCount: '128',
    vCategoryName: 'Hip Hop / R&B',
    vGenreName: 'Hip Hop',
    vTagName: 'Clean',
    vAlbumName: 'The Time',
    inList: null
  }]
}
*/
const saveAlbum = async (data) => {
  const tracks = data.tracks
  const firstTrack = tracks[0]
  const mediaFile = atob(firstTrack.vMediaFile)
  const previewFile = atob(firstTrack.vPreviewFile)
  const type = mediaFile.substring(40, 35)
  let createdAt = moment(firstTrack.dAddedDate, 'MM/DD/YY')
  createdAt = createdAt.isValid() ? createdAt : moment()
  const albumData = {
    where: {
      source: 'bpmsupreme',
      external_id: firstTrack.iAlbumID,
    },
    defaults: {
      title: firstTrack.vAlbumName,
      artist: firstTrack.vArtistName,
      type: type,
      bpm: firstTrack.iBPMCount,
      bpm_range: firstTrack.iBPMCount,
      genre: firstTrack.vGenreName,
      category: firstTrack.vCategoryName,
      created_at: createdAt
    }
  }

  let trackItemResult = await Track.findOrCreate(albumData)
  let trackItem = trackItemResult[0]
  let recordCreated = trackItemResult[1]

  for (let t = 0; t < tracks.length; t++) {
    let track = tracks[t]
    let trackMediaFile = atob(track.vMediaFile)
    let trackPreviewFile = atob(track.vPreviewFile)
    let trackCreatedAt = moment(track.dAddedDate, 'MM/DD/YY')
    let trackData = {
      where: {
        track_id: trackItem.id,
        external_id: track.iMediaID,
      },
      defaults: {
        title: track.vMediaName,
        version_name: track.vTagName,
        artist: track.vArtistName,
        type,
        bpm: track.iBPMCount,
        bpm_range: track.iBPMCount,
        genre: track.vGenreName,
        download_link: trackMediaFile,
        preview_link: trackPreviewFile,
        category: track.vCategoryName,
        created_at: trackCreatedAt.isValid() ? trackCreatedAt : createdAt
      }
    }

    await TrackVersion.findOrCreate(trackData)
  }

  return {
    mediaID: trackItem.id,
    numTracks: tracks.length,
    recordCreated
  }
}

const processFile = (filename) => {
  try {
    var LineByLineReader = require('line-by-line')
    var lr = new LineByLineReader(filename);
    let lineNum = 0
    let saved = 0

    lr.on('line', async (line) => {
      lineNum++

      try {
        var parsedLine = readJsonLine(line)

        if (hasValidTracks(parsedLine)) {
          lr.pause()
          let result = await saveAlbum(parsedLine)
          console.log(chalk.cyan`[SCRAPER_BPM_JSON] saved line=${lineNum} mediaID=${result.mediaID} numTracks=${result.numTracks} mode=${result.recordCreated ? 'created' : 'updated'}`)

          saved++
          lr.resume()
        } else {
          console.log(chalk.yellow`[SCRAPER_BPM_JSON] no tracks found`, lineNum)
        }
      } catch (err) {
        console.error(chalk.red`[SCRAPER_BPM_JSON] error reading line`, lineNum, err)
      }
    })

    lr.on('end', function (line) {
      console.error(chalk.cyan`[SCRAPER_BPM_JSON] work done, saved ${saved} items`)
      process.exit(0)
    })

   } catch (err) {
    console.error(chalk.red`[SCRAPER_BPM_JSON] error during processing`, err)
  }
}

const scrapeBpmSupremeJson = () => {
  console.log('[SCRAPER_BPM_JSON] job start')

  const args = process.argv.slice(2)

  if (!args || args.length !== 2) {
    console.log(chalk.red`[SCRAPER_BPM_JSON] please specify input and output file`)
    process.exit(0)
  }

  const filename = args[0]
  const outputfile = args[1]
  console.log(`[SCRAPER_BPM_JSON] reading from file ${filename}`)

  if (!outputfile) {
    console.error(chalk.red`[SCRAPER_BPM_JSON] no output file specified`)
    process.exit(1)
  }

  fs.stat(filename, async (err, stats) => {
    await timeout(1000)

    if (err) {
      console.error(chalk.red`[SCRAPER_BPM_JSON] error reading file`, err)
    } else {
      processFile(filename)
    }
  })
}

scrapeBpmSupremeJson()
