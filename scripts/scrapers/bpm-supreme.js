require('dotenv').config()

const moment = require('moment')
const Track = require('../../server/models').track
const TrackVersion = require('../../server/models').track_version

const baseUrl = (uri) => 'https://www.bpmsupreme.com' + uri

class bpmClient {
  constructor() {
    this.authenticated = true
    this.cookies = null
    this.request = require('request-promise')
    this.tough = require('tough-cookie')
    this.cookiejar = this.request.jar()
  }

  async authenticate() {
    const vEmail = process.env.SOURCE_BPMSUPREME_EMAIL
    const vPassword = process.env.SOURCE_BPMSUPREME_PASSWORD

    console.log('[SCRAPER_BPM] bpm supreme logging in as', vEmail)
    try {
      const response = await this.request({
        method: 'POST',
        uri: baseUrl('/login/makelogin'),
        jar: this.cookiejar,
        headers: {
          'Referer': 'https://www.bpmsupreme.com/login',
          'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
          'Host': 'www.bpmsupreme.com',
          'DNT': 1,
          'Content-Type': 'application/json;charset=UTF-8',
          'Pragma': 'no-cache'
        },
        body: {
          vEmail,
          vPassword
        },
        json: true
      })

      if (response && response.message === 'Login success!') {
        console.log('[SCRAPER_BPM] bpm supreme login success')
        return true
      } else {
        throw new Error('fail login response')
      }
    } catch (err) {
      console.log('[SCRAPER_BPM] err login', err)
      return false
    }
  }

  async getLatestAlbum (page) {
    console.log('[SCRAPER_BPM] getting album list for page:', page)
    if (!this.authenticated) {
      throw new Error('client unauthenticated')
    }

    try {
      const response = await this.request({
        method: 'POST',
        uri: baseUrl('/store/new_albums/audio/newreleases'),
        jar: this.cookiejar,
        headers: {
          'Referer': 'store/newreleases/audio/classic/1',
          'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
          'Host': 'www.bpmsupreme.com',
          'DNT': 1,
          'Content-Type': 'application/json;charset=UTF-8',
          'Pragma': 'no-cache'
        },
        json: true,
        body: {
          "postData": {
            "bpm_str":"0",
            "bpm_end":"200",
            "vTagCategory":"",
            "sortby":"",
            "sortTrue":"false"
          },
          "pageLimits":0,
          "vCategorySlug":"",
          "view":"classic",
          "page": JSON.stringify(page),
          "tobedownloadList":[]
        }
      })

      if (response &&
        response.mainalbums &&
        response.mainalbums.length &&
        response.mainalbums[0].albums
      ) {
        return response.mainalbums[0].albums
      }
    } catch (err) {
      console.log('[SCRAPER_BPM] getting album error', err)
    }

    return null
  }
}

const timeout = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const saveAlbum = async (album) => {
  console.log('[SCRAPER_BPM] saving album', album)
}

const saveAlbums = async (albums) => {
  console.log('[SCRAPER_BPM] saving albums')

  if (!albums || !Array.isArray(albums) || albums.length === 0) {
    console.log('[SCRAPER_BPM] skipping empty album')
    return null
  }

  console.log(`[SCRAPER_BPM] found ${albums.length} items`)
  for (let i = 0; i < albums.length; i++) {
    var album = albums[i]

    if (!album || !album.tracks || album.tracks.length < 1) {
      console.log('[SCRAPER_BPM] album', album.iAlbumID, 'skip! empty track')
      continue
    }

    console.log('[SCRAPER_BPM] saving album', album.iAlbumID)

    /*
    { iAlbumID: '112849',
    mainiAlbumID: '112849',
    vAlbumName: 'Lyk Dis',
    vAlbumImage: '',
    iBPMCount: '85',
    vArtistName: 'NxWorries, Anderson .Paak & Knxwledge',
    iArtistID: '0',
    vGenreName: 'R&B',
    vGenreSlug: 'r&b',
    iGenre: '72',
    tracks:
     [ { iMediaID: '278009',
         vArtistName: 'NxWorries, Anderson .Paak & Knxwledge',
         iAlbumID: '112849',
         vMediaFile: 'https://s3.amazonaws.com/bpmphase2/audio/NxWorries, Anderson .Paak & Knxwledge - Lyk Dis (Dirty).mp3',
         vMediaName: 'Lyk Dis (Dirty)',
         vVideoThumb: '',
         iAllowDownload: '0',
         dAddedDate: '05/23/18',
         iBPMCount: '85',
         vCategoryName: 'Hip Hop / R&B',
         isSeratoFlip: '0',
         downloadCount: '0',
         inList: '0' }],
    dAddedDate: '05/23/18',
    eAlbumType: 'Audio',
    vTagIDs: 'Dirty,Intro Dirty,Quick Hit Dirty',
    iMediaIDs: '278009,278010,278011',
    artistLink: ' <a title=\'NxWorries\' ng-click=\'trackName(&#36;event,vType);\'>NxWorries</a>, <a title=\' Anderson .Paak \' ng-click=\'trackName(&#36;event,vType);\'>Anderson .Paak</a> & <a title=\' Knxwledge\' ng-click=\'trackName(&#36;event,vType);\'>Knxwledge</a> ',
    vTagID:
     [ { name: 'Dirty', id: '278009', downloadCount: '0' },
       { name: 'Intro Dirty', id: '278010', downloadCount: '0' },
       { name: 'Quick Hit Dirty', id: '278011', downloadCount: '0' } ] }
    */

    try {
      // save track
      let trackItem = {}
      let trackItemResult = await Track.findOrCreate({
        where: {
          source: 'bpmsupreme',
          external_id: album.iAlbumID,
        },
        defaults: {
          title: album.vAlbumName,
          artist: album.vArtistName,
          type: album.eAlbumType,
          bpm: album.iBPMCount,
          bpm_range: album.iBPMCount,
          genre: album.vGenreName,
          category: album.tracks[0].vCategoryName,
          created_at: moment(album.dAddedDate, 'MM/DD/YY')
        }
      })

      trackItem = trackItemResult[0]
      console.log('[SCRAPER_BPM] album saved', album.iAlbumID)
      console.log(`[SCRAPER_BPM] saving ${album.tracks.length} tracks`)

      // save versions
      for (let v = 0; v < album.tracks.length; v++) {
        let trackVersion = album.tracks[v]
        console.log('[SCRAPER_BPM] saving track', trackVersion.iMediaID)

        let trackVersionItem = await TrackVersion.findOrCreate({
          where: {
            track_id: trackItem.id,
            external_id: trackVersion.iMediaID,
          },
          defaults: {
            title: trackVersion.vMediaName,
            version_name: trackVersion.vTagName,
            artist: trackVersion.vArtistName,
            type: album.eAlbumType,
            bpm: trackVersion.iBPMCount,
            bpm_range: trackVersion.iBPMCount,
            genre: album.vGenreName,
            download_link: trackVersion.vMediaFile,
            preview_link: `https://d3cagow29v4w27.cloudfront.net${trackVersion.vMediaFile.substring(34)}`,
            category: trackVersion.vCategoryName,
            created_at: moment(trackVersion.dAddedDate, 'MM/DD/YY')
          }
        })

        console.log('[SCRAPER_BPM] track saved')
      }
    } catch (err) {
      console.log('[SCRAPER_BPM] unable to create track', err)
    }
  }

  return
}

const scrapeBpmSupreme = async () => {
  let started = new Date()
  console.log(`bpm supreme start ${started.toISOString()}`)
  await timeout(1000)

  // get client
  const client = new bpmClient()
  let page = 1

  const authenticated = await client.authenticate()
  if (!authenticated) {
    console.log('[SCRAPER_BPM] Unable to authenticate')
    return
  }

  // loop tru all list
  while (page < 4) {
    // get latest list
    const albums = await client.getLatestAlbum(page)
    await saveAlbums(albums)

    // break;

      // go over to all list
        // check if album exist
          // save album
          // get all track and save

    // timeout
    await timeout(500)

    // increment page
    page++
  }
}

scrapeBpmSupreme()
