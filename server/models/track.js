module.exports = (sequelize, DataTypes) => {
  const Track = sequelize.define('track', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING
    },
    artist: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.STRING
    },
    'created_at': {
      type: DataTypes.STRING
    },
    'external_id': {
      type: DataTypes.INTEGER
    },
    bpm: {
      type: DataTypes.INTEGER
    },
    'bpm_range': {
      type: DataTypes.STRING
    },
    genre: {
      type: DataTypes.STRING
    },
    category: {
      type: DataTypes.STRING
    },
    source: {
      type: DataTypes.STRING
    },
    // 'download_link': {
    //   type: DataTypes.STRING
    // },
    // 'preview_link': {
    //   type: DataTypes.STRING
    // },
    'created_at': {
      type: DataTypes.DATE
    }
  })

  Track.sync().then(() => {})

  return Track
}
