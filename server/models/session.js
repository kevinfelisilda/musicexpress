module.exports = (sequelize, DataTypes) => {
  const Session = sequelize.define('session', {
    sid: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    userId: {
      type: DataTypes.STRING,
      field: 'user_id'
    },
    expires: DataTypes.DATE,
    data: DataTypes.STRING(50000)
  }, {
    timestamps: false
  })

  Session.sync().then(() => {})

  return Session
}
