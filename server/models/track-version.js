module.exports = (sequelize, DataTypes) => {
  const track_version = sequelize.define('track_version', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    track_id: {
      type: DataTypes.INTEGER,
    },
    version_name: {
      type: DataTypes.STRING
    },
    title: {
      type: DataTypes.STRING
    },
    artist: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.STRING
    },
    'created_at': {
      type: DataTypes.STRING
    },
    'external_id': {
      type: DataTypes.INTEGER
    },
    bpm: {
      type: DataTypes.INTEGER
    },
    'bpm_range': {
      type: DataTypes.STRING
    },
    genre: {
      type: DataTypes.STRING
    },
    category: {
      type: DataTypes.STRING
    },
    'download_link': {
      type: DataTypes.STRING
    },
    'preview_link': {
      type: DataTypes.STRING
    },
    'created_at': {
      type: DataTypes.DATE
    }
  })

  track_version.sync().then(() => {})

  track_version.associate = (db) => {
    track_version.belongsTo(db.track, {
      foreignKey: 'track_id',
      as: 'track',
    })
  }

  // Listing.belongsTo(db.Property, {
  //   as: 'property',
  //   foreignKey: 'propertyID',
  //   constraints: false
  // })

  return track_version
}
