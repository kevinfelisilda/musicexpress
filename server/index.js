require('dotenv').config()
const http = require('http')
const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')

const db = require('./models')

// initalize sequelize with session store
var SequelizeStore = require('connect-session-sequelize')(session.Store)

const app = express()
app.use(logger('dev'))
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(session({
  secret: process.env.APP_SECRET,
  store: new SequelizeStore({
    db: db.sequelize,
    table: 'session',
    checkExpirationInterval: 15 * 60 * 1000,
    expiration: 4 * 60 * 60 * 1000,
    extendDefaultFields: (defaults, session) => {
      return {
        data: defaults.data,
        expires: defaults.expires,
        userId: session.userId
      }
    }
  }),
  resave: false, // we support the touch method so per the express-session docs this should be set to false
  // proxy: true // if you do SSL outside of node.
}))

require('./routes')(app)
app.get('*', (req, res) => res.status(200).send({
  message: 'Hi there',
}))

const port = parseInt(process.env.PORT, 10) || 8000
app.set('port', port)

const server = http.createServer(app)
server.listen(port, () => {
  console.log(`The server is running at localhost:${port}`)
})
